from odoo import api
from odoo import models, fields

class StoreMainregion(models.Model):
    _inherit = 'store.mainregion'

    def get_mainregion(self, limit=False, offset=False, order=False, write_date_to=False, write_date_from=False):
        search_list = []
        if write_date_to:
            search_list.append(['write_date','<=',write_date_to])
        if write_date_from:
            search_list.append(['write_date','>=',write_date_from])
        mainregion_ids = self.search(search_list, limit=limit, offset=offset, order=order)
        read_list = [
                    'name',
                    'write_date',
                    'region_ids',
        ]
        result = mainregion_ids.read(read_list, load=False)
        result_count = self.search_count(search_list)
        return result, result_count
