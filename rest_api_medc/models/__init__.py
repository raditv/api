from . import res_user
from . import res_partner
from . import res_store
from . import hr_employee
from . import product_template
from . import product_category
from . import product_pricelist
from . import account_journal

from . import pos_session
from . import pos_order
from . import pos_order_line
from . import pos_informasi

from . import rest_cr
from . import pos_config
from . import pos_promotion
from . import refresh_token

from . import partner_zone
from . import chat
from . import store_mainregion
