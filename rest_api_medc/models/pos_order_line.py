from odoo import api
from odoo import models, fields
#import odoo.addons.decimal_precision as dp

class PosOrderLine(models.Model):
    _inherit = 'pos.order.line'

    def get_order_line(self):
        order_line = self.search([], limit=1000)
        return_dict = [{'product_name' : rec.product_id.name, 'qty' : rec.qty, 'price_unit' : rec.price_unit } for rec in order_line]
        return return_dict
        
        
    @api.model
    def get_lines_api(self, ref):
        result = []
        # order_id = self.search([('pos_reference', '=', ref)], limit=1)
        order_id = self.env['pos.order'].browse(ref)
        if order_id.exists():
            lines = self.search([('order_id', '=', order_id.id)])
            for line in lines:
                if line.qty - line.returned_qty > 0:
                    new_vals = {
                        'product_id': line.product_id.id,
                        'product': line.product_id.name,
                        'qty': line.qty - line.returned_qty,
                        'price_unit': line.price_unit,
                        'discount': line.discount,
                        'line_id': line.id,
                    }
                    result.append(new_vals)
        return result
