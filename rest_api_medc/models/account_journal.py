
from odoo import api
from odoo import models, fields

class PosConfig(models.Model):
    _inherit = 'pos.config'

    def get_journal(self, store_id, limit=False, offset=False, order=False, write_date_to=False, write_date_from=False):
        search_list = [["store_id","=",store_id]]

        pos_config_doc = self.search(search_list, limit=1)
        read_list = [
                        'name',
                        'code',
                        'type',
                        'write_date'
                    ]
        # Without Filtering
        # account_journal = pos_config_doc.journal_ids.read(read_list, load=False)
        # return account_journal, len(pos_config_doc.journal_ids)

        # With Filtering
        account_journal_ids = pos_config_doc.journal_ids.mapped('id')
        search_list = [['id','in',account_journal_ids]]
        if write_date_to:
            search_list.append(['write_date','<=',write_date_to])
        if write_date_from:
            search_list.append(['write_date','>=',write_date_from])
        journal = self.env['account.journal'].search(search_list, limit=limit, offset=offset, order=order)
        result = journal.read(read_list, load=False)

        return result, len(account_journal_ids)
