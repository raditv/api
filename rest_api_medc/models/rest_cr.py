from odoo import api
from odoo import models, fields
#import odoo.addons.decimal_precision as dp
import uuid

class RestCr(models.Model):
    _name = 'rest.cr'

    def login(self, uid, store_id=False):
        result = {}
        res_user_doc = self.env['res.users'].browse(uid)
        res_user = res_user_doc.read(['name', 'store_id', 'store_ids'])[0] #Supaya returnya hanya satu User
        store_ids_list = res_user.get('store_ids')
        if store_id and store_ids_list:
            if not store_id in store_ids_list:
                return False
        #bagian kode ini untuk membaca nama dari Store sebagai pilihan untuk User
        try:
            store_ids = res_user_doc.store_ids.read(['name', 'config_id'])
            res_user['store_ids'] = store_ids
        except:
            pass
        result['res_user']=res_user
        search_list = [['user_id','=', uid],['state','=','granted']]
        refresh_token_doc = self.env['refresh.token'].sudo().search(search_list, limit=1)
        if not refresh_token_doc.exists():
            refresh_token = uuid.uuid1().hex
            self.env['refresh.token'].sudo().create({"name" : refresh_token, "user_id" : uid})
        else :
            refresh_token = refresh_token_doc.name

        result['refresh_token']=refresh_token
        return result

    def get_refresh_token(self, uid, refresh_token):
        result = {}
        uid=int(uid)
        search_list = [['user_id','=', uid],['name','=',refresh_token],['state','=','granted']]
        refresh_token_doc = self.env['refresh.token'].search(search_list, limit=1)
        if not refresh_token_doc.exists():
            return False
        return (refresh_token_doc.user_id.id, refresh_token_doc.user_id.name)

    def pos_init(self, store_id, limit=False):
        #Pos Config
        context = {}
        return_dict = {}
        res_store_doc = self.env['res.store'].browse(store_id)
        if not res_store_doc.exists():
            return False

        pos_config_doc = res_store_doc.config_id

        if not pos_config_doc.exists():
            message = 'Konfigurasi pada Store %s tidak tepat' % (res_store_doc.name)
            return {'error' : {'code': 500, 'message': message}}

        #POS Config Detail
        pos_config_value = pos_config_doc.read(['name','stock_location_id','wk_stock_type','pricelist_id'])[0]
        context['location'] = pos_config_value.get('stock_location_id')[0]
        pricelist_id = pos_config_value.get('pricelist_id')

        context['pricelist'] = pricelist_id
        return_dict['pricelist'] = pricelist_id

        #POS Session
        try:
            pos_session = pos_config_doc.api_open_session_cb()
        except Exception:
            message = 'Sesi pada Store %s harus di close terlebih dahulu' % (res_store_doc.name)
            return {'error' : {'code': 500, 'message': message}}

        return_dict['pos_session_id'] = pos_session

        #1. Product
        # Read Fields
        #"write_date", "id", "name", "lst_price", "pos_categ_id"
        read_fields = ["write_date", "id", "name", "lst_price", "pos_categ_id"]
        context['read_fields'] = read_fields
        product, count = self.env['product.product'].with_context(context).get_product_orm_detail(store_id=store_id)
        return_dict['pos_product'] = product
        return_dict['pos_product_count'] = count

        #2. Product category
        #id, name
        pos_categ_docs = self.env['pos.category'].search([], limit=limit)
        pos_categ = pos_categ_docs.read(['name'])
        return_dict['pos_category'] = pos_categ

        #3. Payment Method
        account_journal = pos_config_doc.journal_ids.read(['name','code','type','journal_user','cash_control'])
        return_dict['account_journal'] = account_journal


        #4. Customer
        #name, KTP, alamat, TO DO : Context branch saja.
        search_filter = [['customer','=',True]]
        res_partner_doc = self.env['res.partner'].search(search_filter, limit=limit)
        read_list = [
                        'name',
                        'street',
                        'city',
                        'zip',
                        'country_id',
                        'email',
                        'phone',
                        'nopol',
                        'pb_barcode'
                    ]
        res_partner = res_partner_doc.read(read_list, load=False)
        return_dict['res_partner'] = res_partner
        return return_dict

    def order_v1(self, body):
        context = {}
        pos_config_id = body.get('pos_config_id')
        if pos_config_id is None:
            return {}
        pos_config_doc = self.env['pos.config'].browse(int(pos_config_id))
        pricelist_id = pos_config_doc.pricelist_id.id
        #1 pos.order.line & pos.order
        orders = body.get('order')
        for order in orders:
            order['pricelist_id'] = pricelist_id
            lines = order.get('lines')
            if lines is None:
                return {}
            new_lines = []
            for line in lines:
                new_lines.append((0, False, line))
            order['lines'] = new_lines
            pos_order_doc = self.env['pos.order'].sudo().create(order)
            #2 stock.move & stock.picking
            #4 account.move.line & account.move
            pos_order_doc.action_paid()
            #To Do : Bank Statement
        result = {}

        return result

    def order(self, body):
        context = {}
        pricelist_id = body.get('pricelist_id')
        context['pricelist'] = pricelist_id
        pos_order_doc = self.env['pos.order'].with_context(context).create_from_ui(body['order'])
        return True
