from odoo import api, fields, models, SUPERUSER_ID, _
from odoo.exceptions import UserError, ValidationError
from ..exceptions import MedcException


class PosSession(models.Model):
    _inherit = 'pos.session'

    def store_session_close(self, session_id, limit=False, steps=2):
        if session_id:
            session_docs = self.browse(session_id)
        else:
            search_list = [["state", "in", ["opened","opening_control","closing_control"]]]
            session_docs = self.search(search_list, limit=limit)
        max_index = len(session_docs)
        for offset in range(0, max_index, steps):
            temp_session = session_docs[offset:offset+steps]
            temp_session.action_pos_session_closing_control()
            self.env.cr.commit()
        message = "Session %s was succesfully closed" % (
            ' , '.join(session_docs.mapped('name')))
        return {'message': message}

    @api.constrains('config_id')
    def _check_pos_config(self):
        session_ids = self.search([
                ('state', '!=', 'closed'),
                ('config_id', '=', self.config_id.id),
                ('name', 'not like', 'RESCUE FOR'),
            ])
        if len(session_ids) > 1:
            troubled_session = ' & '.join(session_ids.mapped('name'))
            raise ValidationError("You cannot create two active sessions related to the same point of sale!. Please check %s " % (troubled_session))