from odoo import api
from odoo import models, fields

class ResUser(models.Model):
    _inherit = 'res.users'
    
    api_write_date = fields.Datetime(string='Latest Change of QT fields')

    def write(self, values):
        res = super(ResUser, self).write(values)
        for rec in self:
            if (values.get('name') or
                values.get('login') or
                values.get('password')
                ):
                rec.api_write_date = fields.Datetime.now()
        return res
