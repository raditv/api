from odoo import models, fields
from odoo.exceptions import UserError, ValidationError
from ..exceptions import MedcException

import json

class ResStore(models.Model):
    _inherit = 'res.store'

    def check_config_id(self):
        pos_config_doc = self.config_id

        if not pos_config_doc.exists():
            message = 'Konfigurasi pada Store %s tidak tepat' % (self.name)
            # return {'error' : {'code': 500, 'message': message}}
            raise MedcException(500, message)

        else:
            return pos_config_doc

    def get_store(self, limit=False, offset=False, order=False, write_date_to=False, write_date_from=False):
        search_list = []
        if write_date_to:
            search_list.append(['write_date', '<=', write_date_to])
        if write_date_from:
            search_list.append(['write_date', '>=', write_date_from])
        store_ids = self.search(search_list, limit=limit,
                                offset=offset, order=order)
        read_list = ['name', 'write_date']
        result = store_ids.read(read_list, load=False)
        result_count = self.search_count(search_list)
        return result, result_count

    def get_store_one(self, store_id):
        store = self.browse(store_id)
        if not store.exists():
            raise MedcException(410, "Data has been permanently deleted")
        config_id = store.check_config_id()

        search_list = [["config_id", "=", config_id.id],
                       ["state", "=", "opened"]]
        sessions = self.env['pos.session'].search(search_list)

        result = {}
        result['id'] = store.id
        result['name'] = store.name
        result['street'] = store.street
        result['street2'] = store.street2
        result['config_id'] = {
            'id': config_id.id,
            'name': config_id.name,
            'receipt_header': config_id.receipt_header,
            'receipt_footer': config_id.receipt_footer,
            'pricelist_id': config_id.pricelist_id.id,
        }
        if not sessions.exists():
            result['session_id'] = False
        else:
            result['session_id'] = {
                'id': sessions[0].id,
                'name': sessions[0].name,
                'start_at': sessions[0].start_at,
                'stop_at': sessions[0].stop_at,
                'sequence_number': sessions[0].sequence_number,
                'login_number': sessions[0].login_number,
            }

        return result

    def get_store_session(self, store_id, session_id=False):
        store = self.browse(store_id)
        if not store.exists():
            raise MedcException(410, "Data has been permanently deleted")
        config_id = store.check_config_id()

        if session_id:
            sessions = self.env['pos.session'].browse(session_id)
        else:
            search_list = [["config_id", "=", config_id.id]]
            sessions = self.env['pos.session'].search(search_list)

        read_list = [
            'write_date',
            'name',
            'id',
            'state',
        ]
        if session_id:
            read_list.extend(['start_at',
                              'stop_at',
                              'sequence_number',
                              'login_number'
                              ])
        result = sessions.read(read_list, load=False)

        return result

    def store_session_close(self, store_id, stop_at=False):
        store = self.browse(store_id)
        if not store.exists():
            raise MedcException(410, "Data has been permanently deleted")
        config_id = store.check_config_id()

        search_list = [["config_id", "=", config_id.id],
                       ["state", "=", "opened"]]
        sessions = self.env['pos.session'].search(search_list)
        if not sessions.exists():
            raise MedcException(406, "No sessions are available to be closed")
        else:
            sessions.action_pos_session_closing_control()
            if stop_at:
                sessions.write({'stop_at': stop_at})
            message = "Session %s in store %s was succesfully closed" % (
                ' & '.join(sessions.mapped('name')), store.name)
            return {'message': message}

    def store_session_open(self, store_id, start_at=False):
        store = self.browse(store_id)
        result = {}
        config_id = store.check_config_id()
        if not config_id.current_session_id:
            new_session_id = self.env['pos.session'].create({
                'user_id': self.env.uid,
                'config_id': config_id.id
            })
            if start_at:
                new_session_id.write({'start_at': start_at})
            config_id.current_session_id = new_session_id
            result['message'] = "New Session"
        else:
            result['message'] = "Existing Session"

        config_id.current_session_id.login()
        result['id'] = store.id
        result['name'] = store.name
        result['config_id'] = {
            'id': config_id.id,
            'name': config_id.name,
            'receipt_header': config_id.receipt_header,
            'receipt_footer': config_id.receipt_footer,
            'pricelist_id': config_id.pricelist_id.id,
        }
        result['session_id'] = {
            'id': config_id.current_session_id.id,
            'name': config_id.current_session_id.name,
            'start_at': config_id.current_session_id.start_at,
            'stop_at': config_id.current_session_id.stop_at,
            'sequence_number': config_id.current_session_id.sequence_number,
            'login_number': config_id.current_session_id.login_number,
        }
        search_list = [['state', '=', 'open'], [
            'pos_session_id', '=', config_id.current_session_id.id]]
        statement_ids = self.env['account.bank.statement'].search(search_list)
        temp_statement_ids = []
        for statement_id in statement_ids:
            data = {}
            data['journal_id'] = statement_id.journal_id.id
            data['name'] = statement_id.journal_id.name
            data['type'] = statement_id.journal_id.type
            data['account_id'] = statement_id.journal_id.default_debit_account_id.id
            data['statement_id'] = statement_id.id
            temp_statement_ids.append(data)
        result['statement_ids'] = temp_statement_ids
        return result

    def get_store_users(self, limit=False, offset=False, order=False, write_date_to=False, write_date_from=False, store_id=False, group=False):
        user_ids_in_store = self.browse(store_id).user_ids.mapped('id')
        search_list = [['id',"in",user_ids_in_store]]
        if write_date_to:
            search_list.append(['api_write_date', '<=', write_date_to])
        if write_date_from:
            search_list.append(['api_write_date', '>=', write_date_from])
        user_ids = self.env['res.users'].search(search_list)    
        if group == "pos_head":
            user_ids = user_ids.filtered(lambda user: user.has_group('base.group_pos_head'))
        read_list = [
            'id',
            'name',
            'login',
            'password_crypt',
            'api_write_date',
        ]
        result = user_ids.read(read_list, load=False)
        return result, len(user_ids)

    def get_store_promotion(self, store_id):
        store = self.browse(store_id)
        if not store.exists():
            raise MedcException(410, "Data has been permanently deleted")
        config_id = store.check_config_id()

        if not config_id.promotion:
            raise MedcException(410, "No promotion")

        promotion_ids = config_id.promotion_ids
        result = {}
        read_list = [
            'name',
            'write_date',
            'start_date',
            'end_date',
            'active']
        result = promotion_ids.read(read_list, load=False)
        return result
        
    def get_store_pricelist(self, store_id, write_date_from=False):
        store = self.browse(store_id)
        config_id = store.check_config_id()
        if not config_id.pricelist_id:
            raise MedcException(410, "No pricelist Configure")
        #Ada Pricelist ID : perubahan pada pricelist yg dipilih
        #Ada Pricelist Write Date : ganti pricelist
        if write_date_from <= config_id.pricelist_id_write_date or write_date_from <= config_id.pricelist_write_date:
            pricelist_id = config_id.pricelist_id
            read_list = ['name','write_date']
            result = pricelist_id.read(read_list, load=False)[0]
            value_json = pricelist_id.check_json_summary()
            value_dict = json.loads(value_json)
            result['item_ids'] = value_dict.get('item_ids', [])
            result['item_count'] = value_dict.get('item_count',0)
            result_count = 1
        else:
            result = []
            result_count = 0
        return result, result_count
