from odoo import api
from odoo import models, fields
#import odoo.addons.decimal_precision as dp

class PosConfig(models.Model):
    _inherit = 'pos.config'

    @api.multi
    def api_open_session_cb(self):
        assert len(self.ids) == 1, "you can open only one session at a time"
        if not self.current_session_id:
            self.current_session_id = self.env['pos.session'].create({
                'user_id': self.env.uid,
                'config_id': self.id
            })
        return [self.current_session_id.id, self.current_session_id.name]
