from odoo import api
from odoo import models, fields

class PosOrder(models.Model):
    _inherit = 'pos.order'


    @api.model
    def get_api_pos_order(self, store_id, query):
        query = query.encode()
        search_list = [ ["store_id","=",store_id],
                        "|", 
                        ["pos_reference","ilike",query],
                        ["return_ref","ilike",query]
                    ]
        pos_order_ids = self.search(search_list, limit=10)
        read_list = [
                        'name',
                        'partner_id',
                        # 'amount_tax',
                        'amount_total',
                        # 'lines',
                        'pos_reference',
                        'return_ref',
                        'return_status',
                        'session_id'
                    ]
        result = pos_order_ids.read_new(read_list)
        return result
        
    # def get_store_order(self, store_id, limit=False, offset=False, order=False, write_date_to=False, write_date_from=False):
    #     search_list = [["store_id","=",store_id]]
    #     if write_date_to:
    #         search_list.append(['write_date','<=',write_date_to])
    #     if write_date_from:
    #         search_list.append(['write_date','>=',write_date_from])
    #     store_ids = self.search(search_list, limit=limit, offset=offset, order=order)
    #     read_list = ['name','write_date']
    #     result = store_ids.read(read_list, load=False)
    #     result_count = self.search_count(search_list)
    #     return result, result_count
