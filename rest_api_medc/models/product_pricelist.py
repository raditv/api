from odoo import api
from odoo import models, fields
#import odoo.addons.decimal_precision as dp
import json

class ProductPricelist(models.Model):
    _inherit = 'product.pricelist'
    
    json_summary = fields.Text(string='JSON Summary')
    

    def get_pricelist(self, limit=False, offset=False, order=False, write_date_to=False, write_date_from=False):
        search_list = [['json_summary', '!=', False]]
        if write_date_to:
            search_list.append(['write_date','<=',write_date_to])
        if write_date_from:
            search_list.append(['write_date','>=',write_date_from])
        pricelist_ids = self.search(search_list, limit=1, offset=offset, order=order)
        read_list = ['name','write_date']
        #Ambil yang terakhir aja
        if pricelist_ids.exists():
            result = pricelist_ids.read(read_list, load=False)[0]
            value_json = pricelist_ids.json_summary
            value_dict = json.loads(value_json)
            result['item_ids'] = value_dict.get('item_ids', [])
            result['item_count'] = value_dict.get('item_count',0)
            result_count = 1
        else:
            result = []
            result_count = 0
        return result, result_count
        
    @api.multi
    def check_json_summary(self):
        if not self.json_summary or self.json_summary == {} :
            json_summary = json.dumps(self.compute_json_summary())
            self.json_summary = json_summary
        else:
            json_summary = self.json_summary
        return json_summary
        
    @api.multi
    def compute_json_summary(self):
        print "Recompute JSON Summary"
        self.ensure_one()
        #Read Item
        read_list = [
            'applied_on',
            'product_tmpl_id',
            'compute_price',
            'fixed_price',
            'min_quantity',
            'date_start',
            'date_end',
        ]
        pricelist = []
        index = 0
        for item in self.item_ids:
            # index +=1
            # if index > 5:
            #     break
            item_vals = item.read(read_list, load=False)
            pricelist.append(item_vals[0])
        vals = {
            'item_ids' : pricelist,
            'item_count' : len(pricelist),
        }
        return vals
        
    #harusnya ngewreite dulu biar dapet yang latest / setelah commit
    #kalau gak, nanti compute_json_summarynya yang versi sebelumnya
    @api.multi
    def write(self, vals):
        if True:
            for rec in self:
                #Recompute JSON jika ada perubahan pada Pricelist Item
                #Agar komputasinya Rest-Api efektif
                res = super(ProductPricelist, self).write(vals)
                if vals.get('item_ids'): 
                    json_summary = rec.compute_json_summary()
                    new_vals ={}
                    new_vals['json_summary'] = json.dumps(json_summary)
                    rec.write(new_vals)
        return res
        
class PosConfig(models.Model):
    _inherit = 'pos.config'
    
    pricelist_write_date = fields.Datetime('Pricelist Changed date')
    pricelist_id_write_date = fields.Datetime(related='pricelist_id.write_date', string='Pricelist Updated date')  

    @api.multi
    def write(self, vals):
        for rec in self:
            if vals.get('pricelist_id'): 
                vals['pricelist_write_date'] = fields.Datetime.now()
            res = super(PosConfig, self).write(vals)
        return res
