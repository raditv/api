from odoo import api
from odoo import models, fields
from ..exceptions import MedcException

class ResPartner(models.Model):
    _inherit = 'res.partner'

    def get_partner(self, limit=False, offset=False, order=False, write_date_to=False, write_date_from=False, partner_id=False):
        read_list = [
                            'write_date',
                            'name',
                            'street',
                            'street2',
                            'city',
                            'phone',
                            'vehicles',
                            'pb_barcode',
                        ]
        if partner_id:
            partner_ids = self.browse(partner_id)
            result = partner_ids.read(read_list, load=False)
            return result, len(partner_ids.exists())
            
        search_list = [['customer','=',True]]
        if write_date_to:
            search_list.append(['write_date','<=',write_date_to])
        if write_date_from:
            search_list.append(['write_date','>=',write_date_from])
        partner_ids = self.search(search_list, limit=limit, offset=offset, order=order)
        result = partner_ids.read(read_list, load=False)
        result_count = self.search_count(search_list)
        return result, result_count

    def edit_partner(self, partner_id, body, **kwargs):
        vals = body.get('data', False)
        uid = kwargs.get('uid', 1) #Supaya ke trace siapa yang create Partner
        partner_id = self.browse(partner_id)
        partner_id.sudo(uid).write(vals)
        message = "Customer %s was succesfully edited" % (
            partner_id.name)
        return {'message': message}
                
    def post_partner(self, body, **kwargs):
        new_partner = body.get('data', False)
        uid = kwargs.get('uid', 1) #Supaya ke trace siapa yang create Partner
        if new_partner :
            new_partner['customer'] = True
            try :
                partner_id = self.sudo(uid).create(new_partner)
                kartu_apung = self.env['ir.sequence'].next_by_code('kartu.apung')
                partner_id.write({'pb_barcode' : kartu_apung})
            except Exception:
                raise MedcException(400, "Invalid parameters to Create New Customer")
        return {
                "partner_id"    :   partner_id.id,
                "pb_barcode"    :   kartu_apung,
                }
