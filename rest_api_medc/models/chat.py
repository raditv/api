from odoo import api
from odoo import models, fields

class Channel(models.Model):
    _inherit = 'mail.channel'

    def get_channel(self):
        result = self.env['mail.channel'].channel_fetch_slot()
        return {'result' : result}
