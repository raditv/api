from odoo import api
from odoo import models, fields
from ..exceptions import MedcException

class ResCountryState(models.Model):
    _inherit = 'res.country.state'

    def get_area(self, limit=False, offset=False, order=False, write_date_to=False, write_date_from=False):
        search_list = []
        if write_date_to:
            search_list.append(['write_date','<=',write_date_to])
        if write_date_from:
            search_list.append(['write_date','>=',write_date_from])
        area_ids = self.search(search_list, limit=limit, offset=offset, order=order)
        read_list = [
                            'id',
                            'name',
                        ]
        result = area_ids.read(read_list, load=False)
        result_count = self.search_count(search_list)
        return result, result_count

class AdministrativeAreaLevel2(models.Model):
    _inherit = 'administrative.area.level2'

    def get_area(self, limit=False, offset=False, order=False, write_date_to=False, write_date_from=False):
        search_list = []
        if write_date_to:
            search_list.append(['write_date','<=',write_date_to])
        if write_date_from:
            search_list.append(['write_date','>=',write_date_from])
        area_ids = self.search(search_list, limit=limit, offset=offset, order=order)
        read_list = [
                            'id',
                            'name',
                            'state_id',
                        ]
        result = area_ids.read(read_list, load=False)
        result_count = self.search_count(search_list)
        return result, result_count

class AdministrativeAreaLevel3(models.Model):
    _inherit = 'administrative.area.level3'

    def get_area(self, limit=False, offset=False, order=False, write_date_to=False, write_date_from=False):
        search_list = []
        if write_date_to:
            search_list.append(['write_date','<=',write_date_to])
        if write_date_from:
            search_list.append(['write_date','>=',write_date_from])
        area_ids = self.search(search_list, limit=limit, offset=offset, order=order)
        read_list = [
                            'id',
                            'name',
                            'admin_area_l2',
                        ]
        result = area_ids.read(read_list, load=False)
        result_count = self.search_count(search_list)
        return result, result_count

class AdministrativeAreaLevel4(models.Model):
    _inherit = 'administrative.area.level4'

    def get_area(self, limit=False, offset=False, order=False, write_date_to=False, write_date_from=False):
        search_list = []
        if write_date_to:
            search_list.append(['write_date','<=',write_date_to])
        if write_date_from:
            search_list.append(['write_date','>=',write_date_from])
        area_ids = self.search(search_list, limit=limit, offset=offset, order=order)
        read_list = [
                            'id',
                            'name',
                            'admin_area_l3',
                            'admin_area_l4',
                        ]
        result = area_ids.read(read_list, load=False)
        result_count = self.search_count(search_list)
        return result, result_count
