from odoo import api
from odoo import models, fields
#import odoo.addons.decimal_precision as dp
from odoo.tools.float_utils import float_round

class ProductTemplate(models.Model):
    _inherit = 'product.template'

    def get_product_orm(self, limit=False, write_date_to=False, write_date_from=False):
        search_list = [['sale_ok','=',True],['available_in_pos','=',True]]
        if write_date_to:
            search_list.append(['write_date','<=',write_date_to])
        if write_date_from:
            search_list.append(['write_date','>=',write_date_from])
        product_ids = self.search(search_list, limit=limit)
        result = product_ids.read(['name', 'list_price'])
        return result

    def get_product_cr(self):
        psql_command = "SELECT name, list_price FROM product_template"
        self._cr.execute(psql_command)
        result = self._cr.fetchall()
        return result

class ProductProduct(models.Model):
    _inherit = 'product.product'

    def get_product_orm(self, limit=False, write_date_to=False, write_date_from=False):
        search_list = [['sale_ok','=',True],['available_in_pos','=',True]]
        if write_date_to:
            search_list.append(['write_date','<=',write_date_to])
        if write_date_from:
            search_list.append(['write_date','>=',write_date_from])
        product_ids = self.search(search_list, limit=limit)
        result = product_ids.read(['name', 'list_price'])
        return result

    def get_product_orm_detail(self, store_id=False, product_id=False, limit=False, offset=False, order=None, write_date_to=False, write_date_from=False):
        context = self.env.context.copy()
        product_ids_detail = False
        product_ids_count = 0
        location_id = self.env['res.store'].browse(store_id).config_id.stock_location_id.id
        if location_id:
            context['location'] = location_id
            if product_id:
                product_ids = self.browse(product_id)
                product_ids_count = 1
                product_ids_detail = product_ids.with_context(context)._product_available_detail()
            else:
                search_list = [['sale_ok', '=', True], ['available_in_pos', '=', True]]
                if write_date_to:
                    search_list.append(['write_date','<=',write_date_to])
                if write_date_from:
                    search_list.append(['write_date','>=',write_date_from])
                product_ids = self.search(search_list, limit=limit, offset=offset, order=order)
                product_ids_count = self.search_count(search_list)
                product_ids_detail = product_ids.with_context(context)._product_available_detail()
        return product_ids_detail, product_ids_count

    @api.multi
    def _product_available_detail(self, field_names=None, arg=False):
        """ Compatibility method """
        return self._compute_quantities_dict_detail(self._context.get('lot_id'), self._context.get('owner_id'), self._context.get('package_id'), self._context.get('from_date'), self._context.get('to_date'))

    @api.multi
    def _compute_quantities_dict_detail(self, lot_id, owner_id, package_id, from_date=False, to_date=False):
        domain_quant_loc, domain_move_in_loc, domain_move_out_loc = self._get_domain_locations()
        domain_quant = [('product_id', 'in', self.ids)] + domain_quant_loc
        dates_in_the_past = False
        if to_date and to_date < fields.Datetime.now(): #Only to_date as to_date will correspond to qty_available
            dates_in_the_past = True

        domain_move_in = [('product_id', 'in', self.ids)] + domain_move_in_loc
        domain_move_out = [('product_id', 'in', self.ids)] + domain_move_out_loc
        if lot_id:
            domain_quant += [('lot_id', '=', lot_id)]
        if owner_id:
            domain_quant += [('owner_id', '=', owner_id)]
            domain_move_in += [('restrict_partner_id', '=', owner_id)]
            domain_move_out += [('restrict_partner_id', '=', owner_id)]
        if package_id:
            domain_quant += [('package_id', '=', package_id)]
        if dates_in_the_past:
            domain_move_in_done = list(domain_move_in)
            domain_move_out_done = list(domain_move_out)
        if from_date:
            domain_move_in += [('date', '>=', from_date)]
            domain_move_out += [('date', '>=', from_date)]
        if to_date:
            domain_move_in += [('date', '<=', to_date)]
            domain_move_out += [('date', '<=', to_date)]

        Move = self.env['stock.move']
        Quant = self.env['stock.quant']
        domain_move_in_todo = [('state', 'not in', ('done', 'cancel', 'draft'))] + domain_move_in
        domain_move_out_todo = [('state', 'not in', ('done', 'cancel', 'draft'))] + domain_move_out
        moves_in_res = dict((item['product_id'][0], item['product_qty']) for item in Move.read_group(domain_move_in_todo, ['product_id', 'product_qty'], ['product_id'], orderby='id'))
        moves_out_res = dict((item['product_id'][0], item['product_qty']) for item in Move.read_group(domain_move_out_todo, ['product_id', 'product_qty'], ['product_id'], orderby='id'))
        quants_res = dict((item['product_id'][0], item['qty']) for item in Quant.read_group(domain_quant, ['product_id', 'qty'], ['product_id'], orderby='id'))
        if dates_in_the_past:
            # Calculate the moves that were done before now to calculate back in time (as most questions will be recent ones)
            domain_move_in_done = [('state', '=', 'done'), ('date', '>', to_date)] + domain_move_in_done
            domain_move_out_done = [('state', '=', 'done'), ('date', '>', to_date)] + domain_move_out_done
            moves_in_res_past = dict((item['product_id'][0], item['product_qty']) for item in Move.read_group(domain_move_in_done, ['product_id', 'product_qty'], ['product_id'], orderby='id'))
            moves_out_res_past = dict((item['product_id'][0], item['product_qty']) for item in Move.read_group(domain_move_out_done, ['product_id', 'product_qty'], ['product_id'], orderby='id'))

        res = dict()
        detailed = self.env.context.get('detailed', False)
        read_fields = self.env.context.get('read_fields', [])
        result = []
        for product in self.with_context(prefetch_fields=False):
            res[product.id] = {}
            if dates_in_the_past:
                qty_available = quants_res.get(product.id, 0.0) - moves_in_res_past.get(product.id, 0.0) + moves_out_res_past.get(product.id, 0.0)
            else:
                qty_available = quants_res.get(product.id, 0.0)
            res[product.id]['qty_available'] = float_round(qty_available, precision_rounding=product.uom_id.rounding)
            # res[product.id]['incoming_qty'] = float_round(moves_in_res.get(product.id, 0.0), precision_rounding=product.uom_id.rounding)
            # res[product.id]['outgoing_qty'] = float_round(moves_out_res.get(product.id, 0.0), precision_rounding=product.uom_id.rounding)
            # res[product.id]['virtual_available'] = float_round(
            #     qty_available + res[product.id]['incoming_qty'] - res[product.id]['outgoing_qty'],
            #     precision_rounding=product.uom_id.rounding)

            for read_field in read_fields:
                try:
                    res[product.id][read_field] = product[read_field]
                    res[product.id]["pos_categ_id"] = product.pos_categ_id.id
                    res[product.id]["uom_id"] = product.uom_id.name
                except:
                    pass
            result.append(res[product.id])
            # Alternative
            # details = product.read(read_fields)
            # merged = merge_two_dicts(details[0], res[product.id])
            # result.append(merged)
        return result

    def get_product_cr(self):
        psql_command = "SELECT name, list_price FROM product_template"
        self._cr.execute(psql_command)
        result = self._cr.fetchall()
        return result
