{
    'name': 'REST API Medc',
    'version': '11.0.1',
    'author': 'MEDC INOVASI DIGITAL',
    'category': 'Backend',
    'website': 'http://medc.co.id/',
    'summary': 'Restful Api Service',
    'description': '''''',
    # 'external_dependencies': {
    #     'python': ['pyjwt'],
    # },
    'depends': [
                'product',
                'stock',
                'point_of_sale',
                'hr',
                'administrative_area_level_1',
                'administrative_area_level_2',
                'administrative_area_level_3',
                'administrative_area_level_4',
                'pos_promotion',
                'pb_informasi_toko',
                'pb_reporting_xlsx', #Store Main Region ada disini
    ],
    'data': [
            'pb_barcode_seq.xml',
            'views/refresh_token.xml'
    ],
    'auto_install': False,
    'installable': True,
    'application': True,
}
