import logging


from odoo import exceptions

class MedcException(Exception):
    def __init__(self, code, message):
        self.code = code
        self.message = message
