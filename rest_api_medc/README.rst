
==================================
REST API Medc
==================================


Idea behind naming
------------------

There are several premium modules which might be used in the future

https://www.odoo.com/apps/modules/10.0/rest_api/

https://apps.odoo.com/apps/modules/9.0/rest_api_drc/



Description
-----------
* The idea is to support Qt client communicate with business-process-packed Odoo engine

**#1 Python Dependency : 'import jwt'**::

    pip install PyJWT
    
Available Routes
================
* GET : /api/partner/
* GET : /api/pricelist/
* GET : /api/product/
* GET : /api/transaction/




Bug Tracker
===========

Credits
=======

Contributors
------------

* Muhammad Fahreza <muhammadfahreza@gmail.com>
