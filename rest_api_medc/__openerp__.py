{
    'name': 'REST API Medc',
    'version': '11.0.1',
    'author': 'MEDC INOVASI DIGITAL',
    'category': 'Backend',
    'website': 'http://medc.co.id/',
    'summary': 'Restful Api Service',
    'description': '''''',
    # 'external_dependencies': {
    #     'python': ['pyjwt'],
    # },
    'depends': ['product', 'stock', 'point_of_sale'],
    'data': [
    ],
    'auto_install': False,
    'installable': True,
    'application': True,
}
