import json

from odoo import http, _
from odoo.http import request, route

from ..jwt.login import token_required


class ApiPartnerZone(http.Controller):

    @route('/api/states', methods=['GET'], type='json', auth='public', csrf=False)
    @token_required()
    def get_area1(self, **kwargs):
        arguments = request.httprequest.args
        limit=arguments.get('limit', False)
        offset=arguments.get('offset', False)
        order=arguments.get('order', 'write_date asc')
        write_date_from=arguments.get('write_date_from', False)
        write_date_to=arguments.get('write_date_to', False)
        if limit:
            limit = int(limit)
        if offset:
            offset = int(offset)
        areas, count = request.env['res.country.state'].sudo().get_area(limit=limit, offset=offset, order=order, write_date_to=write_date_to, write_date_from=write_date_from)
        result = {}
        result['result'] = areas
        result['count'] = count
        return result

    @route('/api/area2', methods=['GET'], type='json', auth='public', csrf=False)
    @token_required()
    def get_area2(self, **kwargs):
        arguments = request.httprequest.args
        limit=arguments.get('limit', False)
        offset=arguments.get('offset', False)
        order=arguments.get('order', 'write_date asc')
        write_date_from=arguments.get('write_date_from', False)
        write_date_to=arguments.get('write_date_to', False)
        if limit:
            limit = int(limit)
        if offset:
            offset = int(offset)
        areas, count = request.env['administrative.area.level2'].sudo().get_area(limit=limit, offset=offset, order=order, write_date_to=write_date_to, write_date_from=write_date_from)
        result = {}
        result['result'] = areas
        result['count'] = count
        return result

    @route('/api/area3', methods=['GET'], type='json', auth='public', csrf=False)
    @token_required()
    def get_area3(self, **kwargs):
        arguments = request.httprequest.args
        limit=arguments.get('limit', False)
        offset=arguments.get('offset', False)
        order=arguments.get('order', 'write_date asc')
        write_date_from=arguments.get('write_date_from', False)
        write_date_to=arguments.get('write_date_to', False)
        if limit:
            limit = int(limit)
        if offset:
            offset = int(offset)
        areas, count = request.env['administrative.area.level3'].sudo().get_area(limit=limit, offset=offset, order=order, write_date_to=write_date_to, write_date_from=write_date_from)
        result = {}
        result['result'] = areas
        result['count'] = count
        return result

    @route('/api/area4', methods=['GET'], type='json', auth='public', csrf=False)
    @token_required()
    def get_area4(self, **kwargs):
        arguments = request.httprequest.args
        limit=arguments.get('limit', False)
        offset=arguments.get('offset', False)
        order=arguments.get('order', 'write_date asc')
        write_date_from=arguments.get('write_date_from', False)
        write_date_to=arguments.get('write_date_to', False)
        if limit:
            limit = int(limit)
        if offset:
            offset = int(offset)
        areas, count = request.env['administrative.area.level4'].sudo().get_area(limit=limit, offset=offset, order=order, write_date_to=write_date_to, write_date_from=write_date_from)
        result = {}
        result['result'] = areas
        result['count'] = count
        return result
