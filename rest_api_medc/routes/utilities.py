import json

from odoo import http, _
from odoo.http import request, route

from ..jwt.login import token_required
        
class ApiTestCron(http.Controller):
    @route('/api/cron/', type='json', methods=['GET'], auth='public', csrf=False)
    def test_cron(self, **kwargs):
        result = request.env["scheduled.pos.session.close"].sudo()._do_pos_session_close()
        return {'result' : {'message': 'Approved'}}
        
class ApiCloseAll(http.Controller):
    @route(route=['/api/session/close',
                  '/api/session/close/<int:session_id>']
                  , type='json', methods=['GET'], auth='public', csrf=False)
    def session_close(self, session_id=False, **kwargs):
        arguments = request.httprequest.args
        limit = arguments.get('limit', False)
        steps = arguments.get('steps', 10)
        if steps:
            steps = int(steps)
        if limit:
            limit = int(limit)
        result = request.env["pos.session"].sudo().store_session_close(session_id, limit, steps)
        return {'result' : result}