import json

from odoo import http, _
from odoo.http import request, route

from ..jwt.login import token_required

class ApiTransaction(http.Controller):
    @route('/api/transaction', type='http', auth='public', csrf=False)
    @token_required()
    def get_transaction(self, debug=False, **k):
        method = request.httprequest.method
        return_value = ""

        if method == "GET":
            result = request.env['pos.order.line'].sudo().get_order_line()
            return_value = json.dumps(result)

        return return_value
