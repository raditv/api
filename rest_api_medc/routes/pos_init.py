import json

from odoo import http, _
from odoo.http import request, route

from ..jwt.login import token_required


class ApiPosInit(http.Controller):
    @route('/api/store/<int:store_id>/open', type='json', methods=['GET'], auth='public', csrf=False)
    @token_required()
    def pos_init(self, store_id, **kwargs):
        arguments = request.httprequest.args
        limit=arguments.get('limit')
        if limit:
            limit = int(limit)
        uid = kwargs['uid']
        result = request.env['rest.cr'].sudo(uid).pos_init(store_id=store_id, limit=limit)
        if not result:
            return {'error' : {'code': 404, 'message': 'Not found'}}
        if result.has_key('error'):
            return result
        return {'result' : result}
