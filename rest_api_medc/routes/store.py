from odoo import http
from odoo.http import request, route

from ..jwt.login import token_required


class ApiStore(http.Controller):

    @route('/api/store', methods=['GET'], type='json', auth='public', csrf=False)
    def get_stores(self, debug=False, **kwargs):
        arguments = request.httprequest.args
        limit = arguments.get('limit', False)
        offset = arguments.get('offset', False)
        order = arguments.get('order', 'name asc')
        write_date_from = arguments.get('write_date_from', False)
        write_date_to = arguments.get('write_date_to', False)
        if limit:
            limit = int(limit)
        if offset:
            offset = int(offset)
        store, count = request.env['res.store'].sudo().get_store(
            limit=limit, offset=offset, order=order, write_date_to=write_date_to, write_date_from=write_date_from)
        result = {}
        result['result'] = store
        result['count'] = count
        return result

    @route('/api/store/<int:store_id>', methods=['GET'], type='json', auth='public', csrf=False)
    def get_store(self, store_id, **kwargs):
        store = request.env['res.store'].sudo().get_store_one(store_id)
        result = {}
        result['result'] = store
        return result

    @route(route=['/api/store/<int:store_id>/sessions/',
                  '/api/store/<int:store_id>/sessions/<int:session_id>'],
           methods=['GET'], type='json', auth='public', csrf=False)
    def get_store_session(self, store_id, session_id=False, **kwargs):
        sessions = request.env['res.store'].sudo(
        ).get_store_session(store_id, session_id)
        result = {}
        result['result'] = sessions
        return result

    @route('/api/store/<int:store_id>/close', methods=['POST'], type='json', auth='public', csrf=False)
    def store_session_close(self, store_id, **kwargs):
        arguments = request.httprequest.args
        stop_at = arguments.get('stop_at', False)
        message = request.env['res.store'].sudo(
        ).store_session_close(store_id, stop_at)
        result = {}
        result['result'] = message
        return result

    @route('/api/store/<int:store_id>/open', methods=['POST'], type='json', auth='public', csrf=False)
    @token_required()
    def store_session_open(self, store_id, **kwargs):
        arguments = request.httprequest.args
        start_at = arguments.get('start_at', False)
        uid = kwargs['uid']
        return_value = request.env['res.store'].sudo(
            uid).store_session_open(store_id, start_at)
        result = {}
        result['result'] = return_value
        return result
        
    @route(route=['/api/store/<int:store_id>/users/',
                  '/api/store/<int:store_id>/users/<string:group>'], methods=['GET'], type='json', auth='public', csrf=False)
    def get_store_users(self, store_id, group=False, **kwargs):
        arguments = request.httprequest.args
        limit = arguments.get('limit', False)
        offset = arguments.get('offset', False)
        order = arguments.get('order', 'name asc')
        write_date_from = arguments.get('write_date_from', False)
        write_date_to = arguments.get('write_date_to', False)
        if limit:
            limit = int(limit)
        if offset:
            offset = int(offset)
            
        users, count = request.env['res.store'].sudo().get_store_users(
            limit=limit, offset=offset, order=order, write_date_to=write_date_to, write_date_from=write_date_from,
            store_id=store_id, group=group)
        result = {}
        result['result'] = users
        result['count'] = count
        return result

    @route('/api/store/<int:store_id>/promotion', methods=['GET'], type='json', auth='public', csrf=False)
    def get_store_promotion(self, store_id, **kwargs):
        promotion = request.env['res.store'].sudo(
        ).get_store_promotion(store_id)
        result = {}
        result['result'] = promotion
        return result
        
    @route('/api/store/<int:store_id>/pricelist', methods=['GET'], type='json', auth='public', csrf=False)
    @token_required()
    def get_store_pricelist(self, store_id, **kwargs):
        arguments = request.httprequest.args
        write_date_from=arguments.get('write_date_from', False)
        pricelist, count = request.env['res.store'].sudo().get_store_pricelist(store_id, write_date_from=write_date_from)
        result = {}
        result['result'] = pricelist
        result['count'] = count
        return result
