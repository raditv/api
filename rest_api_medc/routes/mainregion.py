import json

from odoo import http, _
from odoo.http import request, route

from ..jwt.login import token_required


class ApiMainregion(http.Controller):

    @route('/api/mainregion', methods=['GET'], type='json', auth='public', csrf=False)
    @token_required()
    def get_partner(self, debug=False, **k):
        method = request.httprequest.method
        arguments = request.httprequest.args
        limit=arguments.get('limit', False)
        offset=arguments.get('offset', False)
        order=arguments.get('order', 'write_date asc')
        write_date_from=arguments.get('write_date_from', False)
        write_date_to=arguments.get('write_date_to', False)
        if limit:
            limit = int(limit)
        if offset:
            offset = int(offset)
        mainregion, count = request.env['store.mainregion'].sudo().get_mainregion(limit=limit, offset=offset, order=order, write_date_to=write_date_to, write_date_from=write_date_from)
        result = {}
        result['result'] = mainregion
        result['count'] = count
        return result
