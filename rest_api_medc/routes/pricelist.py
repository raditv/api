import json

from odoo import http, _
from odoo.http import request, route

from ..jwt.login import token_required

class ApiPricelist(http.Controller):
    @route('/api/pricelist', methods=['GET'], type='json', auth='public', csrf=False)
    @token_required()
    def get_pricelist(self, debug=False, **k):
        method = request.httprequest.method
        arguments = request.httprequest.args
        limit=arguments.get('limit', False)
        offset=arguments.get('offset', False)
        order=arguments.get('order', 'write_date asc')
        write_date_from=arguments.get('write_date_from', False)
        write_date_to=arguments.get('write_date_to', False)
        if limit:
            limit = int(limit)
        if offset:
            offset = int(offset)
        pricelist, count = request.env['product.pricelist'].sudo().get_pricelist(limit=limit, offset=offset, order=order, write_date_to=write_date_to, write_date_from=write_date_from)
        result = {}
        result['result'] = pricelist
        result['count'] = count
        return result
        
    @route('/api/pricelist/compute', methods=['GET'], type='json', auth='public', csrf=False)
    @token_required()
    def compute_pricelist(self, debug=False, **k):
        arguments = request.httprequest.args
        limit=arguments.get('limit', False)
        if limit:
            limit = int(limit)
        pricelist_ids = request.env['product.pricelist'].sudo().search([], limit=limit)
        for pricelist_id in pricelist_ids:
            pricelist_id.compute_json_summary()
        result = {}
        return result