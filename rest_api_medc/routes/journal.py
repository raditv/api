import json

from odoo import http, _
from odoo.http import request, route

from ..jwt.login import token_required


class ApiJournal(http.Controller):

    @route('/api/store/<int:store_id>/journal', methods=['GET'], type='json', auth='public', csrf=False)
    @token_required()
    def get_journal(self, store_id=False, **kwargs):
        arguments = request.httprequest.args
        limit=arguments.get('limit', False)
        offset=arguments.get('offset', False)
        order=arguments.get('order', 'write_date asc')
        write_date_from=arguments.get('write_date_from', False)
        write_date_to=arguments.get('write_date_to', False)
        if limit:
            limit = int(limit)
        if offset:
            offset = int(offset)
        journal, count = request.env['pos.config'].sudo().get_journal(store_id=store_id, limit=limit, offset=offset, order=order, write_date_to=write_date_to, write_date_from=write_date_from)
        result = {}
        result['result'] = journal
        result['count'] = count
        return result
