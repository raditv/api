import json

from odoo import http, _
from odoo.http import request, route

from ..jwt.login import token_required


class ApiPosConfig(http.Controller):
    @route('/api/session/<int:pos_config_id>', type='json', methods=['GET'], auth='public', csrf=False)
    @token_required()
    def pos_config(self, pos_config_id, **kw):
        request.env.uid = kw['uid']
        result = request.env['pos.config'].browse(pos_config_id).api_open_session_cb()
        if not result:
            return {'error' : {'code': 404, 'message': 'Not found'}}
        return {'result' : result}
