import json

from odoo import http, _
from odoo.http import request, route

from ..jwt.login import token_required

        
class ApiGetStoreOrder(http.Controller):

    @route('/api/store/<int:store_id>/order', methods=['GET'], type='json', auth='public', csrf=False)
    @token_required()
    def get_store_order(self, store_id=False, **kwargs):
        arguments = request.httprequest.args
        query=arguments.get('query', '')
        order = request.env['pos.order'].sudo().get_api_pos_order(store_id=store_id, query=query)
        result = {}
        result['result'] = order
        return result
        
class ApiPosOrder(http.Controller):
    @route('/api/store/order/', type='json', methods=['POST'], auth='public', csrf=False)
    @token_required()
    def pos_order(self, **kwargs):
        body = request.jsonrequest
        uid = kwargs.get('uid', 1)
        result = request.env['rest.cr'].sudo(uid).order(body)
        return {'result' : {'message': 'Approved'}}
        
class ApiPosHeadAuthorization(http.Controller):
    @route('/api/store/login/', type='json', methods=['POST'], auth='public', csrf=False)
    @token_required()
    def pos_order_authorize(self, **kwargs):
        body = request.jsonrequest
        login = body.get('login', False)
        password = body.get('password', False)
        result = request.env['pos.order'].authorization(True, login, password)
        return {'result' : result}

class ApiPosOrderError(http.Controller):
    @route('/api/error/', type='json', methods=['GET','POST'], auth='public', csrf=False)
    @token_required()
    def error(self, **kw):
        return result
