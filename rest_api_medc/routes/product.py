import json

from odoo import http, _
from odoo.http import request, route

from ..jwt.login import token_required

class ApiProduct(http.Controller):
    @route('/api/productcr', methods=['GET'], type='json', auth='public', csrf=False)
    @token_required()
    def get_product_cr(self, debug=False, **kw):
        return_value = ""
        import timeit
        start = timeit.default_timer()
        product = request.env['product.product'].sudo().get_product_cr()
        stop = timeit.default_timer()
        print 'GET PRODUCT CR'
        print stop - start
        result = {}
        result['result'] = product
        return result

    @route('/api/product', methods=['GET'], type='json', auth='public', csrf=False)
    @token_required()
    def get_product_orm(self, debug=False, **kwargs):
        arguments = request.httprequest.args
        limit=arguments.get('limit', '')
        write_date_from=arguments.get('write_date_from', False)
        write_date_to=arguments.get('write_date_to', False)
        if limit:
            limit = int(limit)
        return_value = ""
        import timeit
        start = timeit.default_timer()
        # product = request.env['product.product'].sudo().get_product_orm(limit=limit, write_date_to=write_date_to, write_date_from=write_date_from)
        product = request.env['product.template'].sudo().get_product_orm(limit=limit, write_date_to=write_date_to, write_date_from=write_date_from)
        stop = timeit.default_timer()
        print 'GET PRODUCT ORM'
        print stop - start
        result = {}
        result['result'] = product
        return result

    # @route('/api/store/<int:store_id>/product', methods=['GET'], type='json', auth='public', csrf=False)
    @route(route=['/api/store/<int:store_id>/product/',
                '/api/store/<int:store_id>/product/<int:product_id>'],
            methods=['GET'], type='json', auth='public', csrf=False)
    @token_required()
    def get_product_orm_detail(self, store_id, product_id=False, **kwargs):
        arguments = request.httprequest.args
        limit=arguments.get('limit', False)
        offset=arguments.get('offset', False)
        order=arguments.get('order', 'write_date asc')
        write_date_from=arguments.get('write_date_from', False)
        write_date_to=arguments.get('write_date_to', False)
        if limit:
            limit = int(limit)
        if offset:
            offset = int(offset)

        image_medium = arguments.get('image_medium', False)
        context = {}
        read_fields = [
                        "write_date",
                        "id",
                        "name",
                        # "barcode",
                        "uom_id",
                        "lst_price"
        ]
        if image_medium:
            read_fields.append("image_medium")
        context['read_fields'] = read_fields
        product, count = request.env['product.product'].with_context(context).get_product_orm_detail(store_id=store_id, product_id=product_id, limit=limit, offset=offset, order=order, write_date_to=write_date_to, write_date_from=write_date_from)
        result = {}
        result['result'] = product
        result['count'] = count
        return result
