import json

from odoo import http, _
from odoo.http import request, route

from ..jwt.login import token_required


class ApiChat(http.Controller):
    @route('/api/chat/channel/', type='json', methods=['GET'], auth='public', csrf=False)
    @token_required()
    def get_channel(self, **kwargs):
        uid = kwargs['uid']
        result = request.env['mail.channel'].sudo(uid).get_channel()
        return result
