import json

from odoo import http, _
from odoo.http import request, route

from ..jwt.login import token_required
        
class ApiGetOrderLine(http.Controller):

    @route('/api/order/<int:order_id>/line', methods=['GET'], type='json', auth='public', csrf=False)
    @token_required()
    def get_order_line(self, order_id=False, **kwargs):
        order = request.env['pos.order.line'].sudo().get_lines_api(ref=order_id)
        result = {}
        result['result'] = order
        return result