import json

from odoo import http, _
from odoo.http import request, route

from ..jwt.login import token_required


class ApiPartner(http.Controller):

    @route(route=['/api/partner/',
                  '/api/partner/<int:partner_id>'],
                  methods=['GET'], type='json', auth='public', csrf=False)
    @token_required()
    def get_partner(self, partner_id=False, debug=False, **k):
        arguments = request.httprequest.args
        limit=arguments.get('limit', False)
        offset=arguments.get('offset', False)
        order=arguments.get('order', 'write_date asc')
        write_date_from=arguments.get('write_date_from', False)
        write_date_to=arguments.get('write_date_to', False)
        if limit:
            limit = int(limit)
        if offset:
            offset = int(offset)
        partner, count = request.env['res.partner'].sudo().get_partner(limit=limit, offset=offset, order=order, write_date_to=write_date_to, write_date_from=write_date_from, partner_id=partner_id)
        result = {}
        result['result'] = partner
        result['count'] = count
        return result

    @route('/api/partner/<int:partner_id>', methods=['PUT'], type='json', auth='public', csrf=False)
    @token_required()
    def edit_partner(self, partner_id, **kwargs):
        body = request.jsonrequest
        result = request.env['res.partner'].edit_partner(partner_id, body, **kwargs)
        return {'result' : result, 'code': 201 }
        
    @route('/api/partner', methods=['POST'], type='json', auth='public', csrf=False)
    @token_required()
    def post_partner(self, **kwargs):
        body = request.jsonrequest
        result = request.env['res.partner'].post_partner(body, **kwargs)
        return {'result' : result, 'code': 201 }
