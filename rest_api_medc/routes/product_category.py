import json

from odoo import http, _
from odoo.http import request, route

from ..jwt.login import token_required

class ApiProductCategory(http.Controller):

    @route('/api/category', methods=['GET'], type='json', auth='public', csrf=False)
    @token_required()
    def get_category(self, debug=False, **kw):
        method = request.httprequest.method
        arguments = request.httprequest.args
        limit=arguments.get('limit', False)
        offset=arguments.get('offset', False)
        order=arguments.get('order', 'write_date asc')
        write_date_from=arguments.get('write_date_from', False)
        write_date_to=arguments.get('write_date_to', False)
        if limit:
            limit = int(limit)
        if offset:
            offset = int(offset)
        category, count = request.env['pos.category'].sudo().get_category(limit=limit, offset=offset, order=order, write_date_to=write_date_to, write_date_from=write_date_from)
        result = {}
        result['result'] = category
        result['count'] = count
        return result
