from odoo import api
from odoo import models, fields

class PosInformasi(models.Model):
    _name = 'pos.informasi'

    name = fields.Char('Judul', copy=True)
    content = fields.Text('Pengumuman', copy=True)
    user_id = fields.Many2one('res.users', 'Responsible Person', default=lambda self: self.env.user)
    date_start = fields.Datetime('Berlaku mulai', default=fields.Datetime.now(), required=1)
    date_end = fields.Datetime('Berlaku hingga')
    sequence = fields.Integer('Sequence', index=True)

    active = fields.Boolean(default=True)
    
    nasional = fields.Boolean(default=False)
    
    company_ids = fields.Many2many(
        'res.company',
        'pos_informasi_company',
        'company_id', 'pos_informasi',)
        
    mainregion_ids = fields.Many2many(
        'store.mainregion',
        'pos_informasi_mainregion',
        'mainregion_id', 'pos_informasi',)     
        
    region_ids = fields.Many2many(
        'store.region',
        'pos_informasi_region',
        'region_id', 'pos_informasi',)
                
    store_ids = fields.Many2many(
        'res.store',
        'pos_informasi_store',
        'store_id', 'pos_informasi',)