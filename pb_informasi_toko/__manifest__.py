{
    'name': 'Informasi Toko - POS Offline',
    'version': '11.0.1',
    'author': 'MEDC INOVASI DIGITAL',
    'category': 'Backend',
    'website': 'http://medc.co.id/',
    'summary': 'Pengumuman untuk POS Offline',
    'description': '''''',
    'data': [
            'views/pos_informasi.xml'
    ],
    'auto_install': False,
    'installable': True,
    'application': True,
}
